package br.com.simonini.coalawebsite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoalaWebsiteApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoalaWebsiteApplication.class, args);
	}
}
